﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(angular4mvcmongo.Startup))]
namespace angular4mvcmongo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
